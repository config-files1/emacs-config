# Emacs Config

My configuration of Emacs

## Setup Instructions for Linux
Go to the root of the repository
```
cd emacs-config
```

Copy my config to the Emacs config directory
```
cp -v init.el ~/.emacs.d/init.el 
```

## Setup Instructions for Windows
It is recommended to install Emacs via Scoop
```
scoop bucket add extras
scoop install emacs
```

Go to the root of the repository
```
cd emacs-config
```

Copy my config to the Emacs config directory
```
cp -v init.el ~\AppData\Roaming\.emacs.d\init.el
```

## Emacs plugins
* [doom-modeline](https://github.com/seagle0128/doom-modeline)
* [flycheck](https://github.com/flycheck/flycheck)
* [company-mode](https://github.com/company-mode/company-mode)
* [rust-mode](https://github.com/rust-lang/rust-mode)
* [go-mode.el](https://github.com/dominikh/go-mode.el)
* [projectile](https://github.com/bbatsov/projectile)
* [treemacs](https://github.com/Alexander-Miller/treemacs)
	* [treemacs-projectile](https://github.com/Alexander-Miller/treemacs/blob/master/src/extra/treemacs-projectile.el)
	* [treemacs-icons-dired](https://github.com/Alexander-Miller/treemacs/blob/master/src/extra/treemacs-icons-dired.el)
* [markdown-mode](https://github.com/jrblevin/markdown-mode)
